import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/common/jwt.service";
import { API_URL } from "@/common/config";

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  },

  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Token ${JwtService.getToken()}`;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  get(resource, slug = "") {
    return Vue.axios.get(`${resource}/${slug}`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },
  getAuth(resource) {
    return Vue.axios.post(`${resource}/authentication/`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  post(resource, params) {
    const qs = require("qs");
    return Vue.axios.post(`${resource}`, qs.stringify(params.user), {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    });
  },
  clearPost(resource) {
    return Vue.axios.post(`${resource}`, "logout", {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    });
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  }
};

export default ApiService;

export const TagsService = {
  get() {
    return ApiService.get("tags");
  }
};

export const ArticlesService = {
  query(type, params) {
    return ApiService.query("guest/getrestaurants", {
      params: params
    });
  },
  get(slug) {
    return ApiService.get("guest/getrestaurant", slug);
  },
  create(params) {
    return ApiService.post("articles", { article: params });
  },
  update(slug, params) {
    return ApiService.update("articles", slug, { article: params });
  },
  destroy(slug) {
    return ApiService.delete(`articles/${slug}`);
  }
};

export const RestsService = {
  query(type, params) {
    return ApiService.query("guest/getrestaurants", {
      params: params
    });
  },
  get(slug) {
    return ApiService.get("articles", slug);
  },
  create(params) {
    return ApiService.post("articles", { article: params });
  },
  update(slug, params) {
    return ApiService.update("articles", slug, { article: params });
  },
  destroy(slug) {
    return ApiService.delete(`articles/${slug}`);
  }
};

export const CommentsService = {
  get(slug) {
    if (typeof slug !== "string") {
      throw new Error(
        "[RWV] CommentsService.get() article slug required to fetch comments"
      );
    }
    return ApiService.get("guest/getrestaurant", `${slug}`);

    // /comments
  },

  post(slug, payload) {
    return ApiService.post(`user/checkin/${slug}`, {
      comment: { body: payload }
    });
  },

  destroy(slug, commentId) {
    commentId.toString();
    return ApiService.delete(`guest/getrestaurant/${slug}`);
  }
};
export const ReviewService = {
  get(slug) {
    if (typeof slug !== "string") {
      throw new Error(
        "[RWV] CommentsService.get() article slug required to fetch comments"
      );
    }
    return ApiService.get("guest/getrestaurant", `${slug}`);

    // /comments
  },

  post(slug, payload) {
    return ApiService.post(`user/review/${slug}`, {
      review: { body: payload }
    });
  },

  destroy(slug, commentId) {
    commentId.toString();
    return ApiService.delete(`guest/getrestaurant/${slug}`);
  }
};
export const FavoriteService = {
  add(slug) {
    return ApiService.post(`articles/${slug}/favorite`);
  },
  remove(slug) {
    return ApiService.delete(`articles/${slug}/favorite`);
  }
};
export const VisitsService = {
  query(type, params) {
    type.toString();
    params.toString();
    return ApiService.query("user/getActiveVisit");
  },
  get(slug) {
    return ApiService.get("articles", slug);
  },
  create(params) {
    return ApiService.post("articles", { article: params });
  },
  update(slug, params) {
    return ApiService.update("articles", slug, { article: params });
  },
  destroy(slug) {
    return ApiService.delete(`articles/${slug}`);
  }
};
//aa

export const MenuService = {
  query(type, params) {
    return ApiService.query("guest/getrestaurants", {
      params: params
    });
  },
  get(slug) {
    return ApiService.get("guest/getrestaurant", slug);
  },
  create(params) {
    return ApiService.post("articles", { article: params });
  },
  update(slug, params) {
    return ApiService.update("articles", slug, { article: params });
  },
  destroy(slug) {
    return ApiService.delete(`articles/${slug}`);
  }
};
